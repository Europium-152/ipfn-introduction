# IPFN Introduction

This is an introduction to setting up your work environment when working with ASDEX Upgrade (AUG) data for the Instituto de Plasmas e Fusão Nuclear (IPFN) reflectometry group.

This guide will help you configure your work environment on the AUG Andrew File System (AFS). 
This will enable you to do data processing and data visualization tasks remotely, leveraging the computing power offered by the AUG clusters.

* [Preparing for remote access](#preparing-for-remote-access)
    * [Connecting via SSH](#connecting-via-ssh)
    * [Connecting via VNC](#connecting-via-vnc)
* [Using python for data processing](#using-python-for-data-processing)
    * [Obtaining your copy of `ipfnpytools`](#obtaining-your-copy-of--ipfnpytools-)
    * [Configuring your Jupyter server](#configuring-your-jupyter-server)
* [Accessing MPCDF Without a VPN](#accessing-mpcdf-without-a-vpn)
* [The End](#the-end)

## Preparing for remote access

To get started, we will need two things.

* A connection to the AUG Virtual Private Network (VPN).
* Software to connect via Secure Shell (SSH) and Virtual Network Computing (VNC).

To set up the VPN, follow the instructions on [the AUG website](https://www.mpcdf.mpg.de/services/campus/vpn).

For **Windows users**, there is a very good SSH and VNC client [MobaXterm](https://mobaxterm.mobatek.net/download.html) for which there is a free version that is perfect for us.

If you which to access an MPCDF cluster without connecting to the VPN, jump to section [Accessing MPCDF without a VPN](#accessing-mpcdf-without-a-vpn)

### Connecting via SSH

AUG provides several [computing servers](https://www.aug.ipp.mpg.de/wwwaug/documentation/computerIT/Server.html). 
You can connect to one of them via SSH `ssh -X <username>@sxaug23.aug.ipp.mpg.de`.
The `-X` parameter gives you access to software with a Graphical User Interface (GUI), e.g., `diaggeom` and `DIVERTOR`, the `<username>` should be replaced with your AFS username.

It is straightforward to set up an SSH connection with MobaXterm. First, open MobaXterm then hit the top menu Sessions>New Session.
Choose session type SSH and on "remote host" write `sxaug23.aug.ipp.mpg.de`. Check the box that says "Specify username" and fill in your AFS username.
Once you hit OK, an SSH session will begin, and you will be prompted for your AFS password. You will also be given the option to memorize the password, which is a convenient way of starting your next SSH session.
To exit the SSH session, just close the newly opened tab.

MobaXterm saves your sessions and puts them on a list for quick access, which is very convenient. You should see your newly opened session under the name `sxaug23.aug.ipp.mpg.de (username)` on the left-hand side of the "Home" tab.

![User sessions](images/mobaxterm_user_sessions.png)

To test that everything is working, try writing `diaggeom` on the terminal. After waiting for a few minutes, you will see a window appear.
Additionally, you will see that your AFS files all appear on your left-hand side. MobaXterm allows you to copy files between your AFS and Windows systems with a drag-and-drop action which is very lovely :)

![Diaggeom](images/open_diaggeom.png)

### Connecting via VNC

Although connecting via SSH is pretty straightforward, it is not the fasted nor best way to use AUG applications such as `diaggeom` or `DIVERTOR`.

VNC allows a faster connection and has the advantage of being persistent. You can shut down your PC or change between PC's and the VNC session will persist with all the windows opened where you left them.

The Max Planck Computing and Data Facility (MPCDF) provides a series of [Linux clusters](https://tok.ipp.mpg.de/wiki/TOK/index.php/Computing-LinuxClusters), 
some of which are dedicated to interactive use such as the [TOKI cluster](https://tok.ipp.mpg.de/wiki/TOK/index.php/Computing-TOKI).

To open a VNC session on TOKI, you will first need to connect via SSH to one of the nodes `toki01.bc.rzg.mpg.de` ... `toki08.bc.rzg.mpg.de`. 
Once you are connected, you will need to add the following [xstartup file](files/xstartup) to the `.vnc` folder on your AFS home directory. 
If you do not have a `.vnc` folder, you will need to create one. 
Make sure that the file is named exactly `xstartup` and that it is executable. 
To make this file executable, climb to the `.vnc` directory and run:

```xterm
$ chmod u+x xstartup
```

This file will automatically configure the desktop of your VNC sessions. 
The last thing before you open your VNC session is setting up a password, on the terminal write `vncpasswd` and enter your desired password for the VNC sessions.

Before creating a VNC session, you should always check to see if you have any current VNC sessions. 
Running `vncserver -list` will display a list of your current VNC sessions.

```xterm
user@toki01[~] vncserver -list

TigerVNC server sessions:

X DISPLAY #     PROCESS ID
user@toki01[~]
```

To create a new VNC session, you need only specify the desired resolution. For most modern computers, this will be 1920x1080, but you should double-check your screen resolution, just in case. 
The VNC session will be opened on a port of the TOKI node, e.g., `toki01.bc.rzg.mpg.de:5948` for port 48 as in the code snippet below.
**You should run the following command from your root directory:**

```xterm
user@toki01[~] vncserver -geometry 1920x1080

New 'toki01:48 (user)' desktop is toki01:48

Starting applications specified in /afs/ipp-garching.mpg.de/home/u/user/.vnc/xstartup
Log file is /afs/ipp-garching.mpg.de/home/u/user/.vnc/toki01:48.log

user@toki01[~]
```

Running `vncserver -list` will now show the newly created VNC session.

```xterm
user@toki01[~] vncserver -list

TigerVNC server sessions:

X DISPLAY #     PROCESS ID
:48             4867
user@toki01[~]
```

To connect to the VNC session using MobaXterm, click on Sessions>New Session and select the VNC option.
On "Remote hostname or IP address," write the address of the TOKI node you are using `toki01.bc.rzg.mpg.de` ... `toki08.bc.rzg.mpg.de`.
On the "Port" option, select the port where the VNC was created, e.g., 5948 for port 48, 5903 for port 3, etc.
After clicking OK, you will be prompted for your password, which you can choose to remember.
Once you connect to the VNC session, you will be greeted by your desktop. Make sure to enter fullscreen mode. 
Once in full-screen mode, you can view the MobaXterm toolbar by pushing your mouse cursor against the top of the screen.

![Entering full-screen mode](images/enter_fullscreen.png)

Now you can run `diaggeom` and other applications by opening the terminal inside the VNC session.
There is just a small caveat, `diaggeom`, and other applications do not run on the TOKI cluster. Hence, you need to connect to an AUG machine from inside the VNC session by running `ssh -X sxaug33`.
From here, you can call `diaggeom`[^1].

![Opening diaggeom through VNC](images/diaggeom_sxaug.png)

If you need to close the VNC window at any point, the session will remain unchanged, and you can reenter whenever you want.
You can finish the VNC session by running `vncserver -kill :48`, replacing 48 by the port number of your session. Go ahead and kill the VNC you just created.

```xterm
user@toki01[~] vncserver -kill :48
Killing Xvnc process ID 4867
user@toki01[~]
```

## Using python for data processing

One of the better ways to use python on AUG is by using [jupyter notebooks](https://jupyter.org/). 
Jupyter creates a python kernel that runs on AUG (typically on a TOKI machine) and interfaces through a network port.
Jupyter can also be configured to allow remote access. This means that the python kernel can run on TOKI while the interface runs on your web browser. Fantastic.

MPCDF provides software in [modules](https://www.mpcdf.mpg.de/services/computing/software/modules), modules can be loaded using the `module load` command. 
Some modules might always be useful to have at hand, git, python, and others. 

You can automatically configure which modules are loaded at login time using this [.tcshrc file](https://gitlab.com/Europium-152/ipfn-introduction/-/blob/master/files/.tcshrc). 
Add the linked file to your AFS root directory.

If you already have a .tcshrc file and do not which to replace it, then go to the file provided in the link and find the following parts to copy to your version of the .tcshrc

* Under the text `case tok*:` find and copy this code
```txt
 case tok*:
    module purge
    module load anaconda/2
    module load git
    module load ffmpeg
    setenv PYTHONPATH /afs/ipp/aug/ads-diags/common/python/lib
    setenv PYTHONPATH "$PYTHONPATH":/afs/ipp-garching.mpg.de/u/$USER/python/ipfnpytools
    breaksw
```

* And again near the end of the file
```txt
## Replace with your username below
setenv PATH /afs/ipp-garching.mpg.de/u/$USER/bin:$PATH
```

This will automatically load some modules when you log in to a TOKI cluster (anaconda/2, git, and ffmpeg). 
It will also point your python path to two essential directories, the AUG python library, and your own personal copy of ipfnpytools.
For this configuration to have an effect, you need to restart your session, so go ahead and close the SSH session and open it again. 
As an alternative, you can run the following command from your root directory:

```xterm
$ source .tcshrc
```

### Obtaining your copy of `ipfnpytools`

Go to your AFS home directory and create a folder called `python`. Then, clone `ipfnpytools` into the directory you just created. Easy peasy.

```xterm
user@toki01[~] mkdir python

user@toki01[~] cd python

user@toki01[~/python] git clone https://gitlab.com/ipfn-reflectometry/ipfnpytools.git

Cloning into 'ipfnpytools'...
remote: Enumerating objects: 70, done.
remote: Counting objects: 100% (70/70), done.
remote: Compressing objects: 100% (43/43), done.
remote: Total 2362 (delta 42), reused 38 (delta 27), pack-reused 2292
Receiving objects: 100% (2362/2362), 485.69 KiB | 151.00 KiB/s, done.
Resolving deltas: 100% (1708/1708), done.

user@toki01[~/python] cd ipfnpytools

user@toki01[ipfnpytools] git checkout lab_rot

Branch 'lab_rot' set up to track remote branch 'lab_rot' from 'origin'.
Switched to a new branch 'lab_rot'

```

### Configuring your Jupyter server

The last step before getting crazy with python is to configure your remote Jupyter server so that you can work remotely from your web browser.

But first, we need to set up secure communication with the jupyter notebook using SSL.
For more information on setting up a notebook server, visit [this link](https://jupyter-notebook.readthedocs.io/en/stable/public_server.html).
For our purposes, you just need to follow these steps.

* Create a new directory in your AFS root and run the SSL certificate generator
    ```xterm
    $ mkdir .ssl
    $ cd .ssl
    $ openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout mykey.key -out mycert.pem
    ```
    
* Create the jupyter configuration file. The configuration is stored under `.jupyter/jupyter_notebook_config.py` 
    ```xterm
    $ jupyter notebook --generate-config
    ```

* Open the configuration file and delete all the lines in the file and replace them with the following text, **replacing** `YOUR_USERNAME` **with your AFS username**.
    ```python
    c.NotebookApp.allow_remote_access = True
    
    c.NotebookApp.certfile = u'/afs/ipp-garching.mpg.de/u/YOUR_USERNAME/.ssl/mycert.pem'
    c.NotebookApp.keyfile = u'/afs/ipp-garching.mpg.de/u/YOUR_USERNAME/.ssl/mykey.key'
    
    c.NotebookApp.ip = '*'
    c.NotebookApp.open_browser = False
    
    c.NotebookApp.port = 9999
    ```

* Create a password for your jupyter notebooks. The password is hashed and stored in `.jupyter/jupyter_notebook_config.json`
    ```xterm
    $ jupyter notebook password
    ```
    
* Go to your root directory and create a new folder with a cool name, e.g., `notebooks`. From within this folder, launch the jupyter server.
    ```xterm
    $ cd
    $ mkdir notebooks
    $ cd notebooks
    $ jupyter notebook
    
    [I 16:59:28.036 NotebookApp] Writing notebook server cookie secret to /run/user/13655/jupyter/notebook_cookie_secret
    [I 16:59:28.900 NotebookApp] JupyterLab extension loaded from /mpcdf/soft/SLE_15/packages/x86_64/anaconda/2/2019.03/lib/python2.7/site-packages/jupyterlab
    [I 16:59:28.900 NotebookApp] JupyterLab application directory is /afs/ipp-garching.mpg.de/common/soft/obs/SLE-15-sandybridge/soft/SLE_15/packages/x86_64/anaconda/2/2019.03/share/jupyter/lab
    [I 16:59:31.697 NotebookApp] Serving notebooks from local directory: /afs/ipp-garching.mpg.de/home/d/danielhfc
    [I 16:59:31.698 NotebookApp] The Jupyter Notebook is running at:
    [I 16:59:31.698 NotebookApp] https://(toki01 or 127.0.0.1):9999/
    [I 16:59:31.698 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).

    ```
* Open jupyter on your browser. The penultimate line of the output shows the network port of the jupyter server. 
**Pay attention** to the digits after the colon. 
In this case, jupyter is under the URL `https://toki01.bc.rzg.mpg.de:9999` but it could be in another port, e.g., `https://toki01.bc.rzg.mpg.de:10000`.

* On your browser, create a new python 2 notebook

![Create a new notebook](images/new_notebook.png)

* Copy and paste these lines of code into the jupyter cell and hit `Shift` + `Return`. If everything went well, you should be looking at something like:

```python
%matplotlib notebook

from ipfnpytools.plot import plot_signals
from ipfnpytools.aug_read import many_signals

data = many_signals('FPC', 'IpiFP', 37765)
plot_signals(data=data);
```

![A plot using python](images/my_first_plot.png)

## Accessing MPCDF without a VPN

To manage our shell sessions on the MPCDF clusters we will use tmux. For this purpose, you should create the configuration file `~\.tmux.config` on your MPCDF file system with the following line inside the file:
```
set -g default-command $SHELL
```

1. You can access mpcdf infrastructure without a VPN by connecting through the gateway server `gate.mpcdf.mpg.de`. On you terminal type:
```xterm
    $ ssh YOUR_USERNAME@gate.mpcdf.mpg.de
```

From here you can ssh to any machine within the MPCDF network. Next we'll see how to set up a jupyter notebook that you can run from your local machine.

2. ssh to a toki cluster (from toki01 to toki08)
```xterm
    $ ssh YOUR_USERNAME@tokiXX.bc.rzg.mpg.de
```
3. Open a tmux session
```xterm
    $ tmux
```
4. Run jupyter without a browser.
```xterm
    $ jupyter notebook --no-browser --port JUPYTER_PORT
```
`JUPYTER_PORT` should be a valid port number, like 9999. You don't actually need to specify the port in which case it defaults to 9999

5. Leave/detach the tmux session by typing `Ctrl`+`b` and then `d`.

6. Exit both ssh sessions by typing `exit` twice. Or open a new terminal on your local machine

7. Create an ssh tunnel between your localhost and the gateway and then between the gateway and the toki cluster where your notebook is running.
```xterm
    $ ssh -L 9999:localhost:EMPTY_PORT YOUR_USERNAME@gate.mpcdf.mpg.de
    $ ssh -L EMPTY_PORT:localhost:1234 -N YOUR_USERNAME@tokiXX.bc.rzg.mpg.de
```
The first command creates a tunnel between **your** localhost:9999 to the localhost:EMPTY_PORT at gate.mpcdf.mpg.de
The second command creates a tunnel between the gateway's localhost:EMPTY_PORT and the localhost:JUPYTER_PORT at tokiXX.bc.rzg.mpg.de
You can now open your favorite browser and open the jupyter notebook by typing `localhost:9999` on the URL bar. 

You can close the jupyter notebook running in the toki machine by repeating steps 1 and 2 followed by
```xterm
    $ tmux attach
```


## The End

Congratulations on finishing the IPFN tutorial on setting up your working environment. 
To get started with data analysis, visit [this repository](https://github.com/Europium-152/AUG) and download some of the notebooks.

## 

[^1]: I know what you are thinking, I have to connect to TOKI through SSH to open a VNC session from which I will have to connect to an sxaug machine through SSH to open diaggeom? Is this really the best way? To my knowledge, yes.
